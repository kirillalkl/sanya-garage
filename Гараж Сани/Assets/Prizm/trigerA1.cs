﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trigerA1 : MonoBehaviour
{
    public GameObject raduga;


    // Start is called before the first frame update
    void Start()
    {
        raduga.SetActive(false);
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Finish")
        {
            
            raduga.SetActive(true); 
        }
        
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Finish")
        {

            raduga.SetActive(false);
        }

    }

}
